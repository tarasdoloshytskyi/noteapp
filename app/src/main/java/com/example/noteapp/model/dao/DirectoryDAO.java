package com.example.noteapp.model.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.noteapp.model.entities.Directory;

import java.util.List;

@Dao
public interface DirectoryDAO {

    @Query("SELECT * FROM Directory")
    List<Directory> getAllDirectories();

    @Insert
    void insert(Directory directory);

    @Delete
    void delete(Directory directory);

    @Update
    void update(Directory directory);
}
