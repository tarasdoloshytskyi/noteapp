package com.example.noteapp.model.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.example.noteapp.model.dao.DirectoryDAO;
import com.example.noteapp.model.dao.NoteDAO;
import com.example.noteapp.model.entities.Directory;
import com.example.noteapp.model.entities.Note;

@Database(entities = {Directory.class, Note.class}, version = 4)
public abstract class AppDatabase extends RoomDatabase {
    public abstract DirectoryDAO directoryDAO();
    public abstract NoteDAO noteDAO();
}
