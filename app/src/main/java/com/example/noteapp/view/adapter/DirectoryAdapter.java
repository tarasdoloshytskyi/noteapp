package com.example.noteapp.view.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.noteapp.R;
import com.example.noteapp.model.entities.Directory;

import java.util.ArrayList;

public class DirectoryAdapter extends RecyclerView.Adapter<DirectoryAdapter.DirectoryViewHolder> {

    private ArrayList<Directory> directories;
    private OnItemClickListener clickListener;

    public interface OnItemClickListener {
        void onItemClick(int position);

        void onDeleteClick(int position);
    }

    public class DirectoryViewHolder extends RecyclerView.ViewHolder {

        public ImageButton ibDelete;
        public TextView tvName;

        public DirectoryViewHolder(@NonNull final View itemView, final OnItemClickListener listener) {
            super(itemView);
            ibDelete = itemView.findViewById(R.id.ib_directory_delete);
            tvName = itemView.findViewById(R.id.tv_directory_name);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }
                }
            });

            ibDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onDeleteClick(position);
                        }
                    }
                }
            });
        }
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        clickListener = listener;
    }

    public DirectoryAdapter(ArrayList<Directory> directories) {
        this.directories = directories;
    }

    @NonNull
    @Override
    public DirectoryViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.directory_item, viewGroup, false);
        DirectoryViewHolder directoryViewHolder = new DirectoryViewHolder(view, clickListener);
        return directoryViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull DirectoryViewHolder directoryViewHolder, int i) {
        Directory directory = directories.get(i);
        directoryViewHolder.tvName.setText(directory.getName());
    }

    @Override
    public int getItemCount() {
        return directories.size();
    }
}
