package com.example.noteapp.view.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.noteapp.R;
import com.example.noteapp.model.entities.Directory;
import com.example.noteapp.model.entities.Note;
import com.example.noteapp.view.adapter.DirectoryAdapter;
import com.example.noteapp.view.adapter.NoteAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListNotesFragment extends BaseFragment {

    private RecyclerView rvListNotes;
    private NoteAdapter adapter;

    public ListNotesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list_notes, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rvListNotes = getActivity().findViewById(R.id.rv_list_directories);
        rvListNotes.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        rvListNotes.setLayoutManager(layoutManager);
        adapter = new NoteAdapter(mActivity.notes);
        rvListNotes.setAdapter(adapter);
        adapterClickListener();
    }

    private void adapterClickListener() {
        adapter.setOnItemClickListener(new DirectoryAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Toast.makeText(getActivity(), mActivity.notes.get(position).getTitle(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onDeleteClick(int position) {
                Note note = mActivity.notes.get(position);
                deleteItem(position, note);
            }
        });
    }

    private void deleteItem(final int position, final Note note) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                database.noteDAO().deleteNote(note);
            }
        }).start();
        mActivity.notes.remove(position);
        adapter.notifyItemRemoved(position);
    }
}
