package com.example.noteapp.view.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.noteapp.MainActivity;
import com.example.noteapp.R;
import com.example.noteapp.model.db.AppDatabase;
import com.example.noteapp.model.entities.Directory;

import java.util.ArrayList;

public class NewDirectoryFragment extends BaseFragment {

    private EditText etName;
    private Button btnAddDirectory;


    public NewDirectoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_new_directory, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setInitialData();
        initOnClickListener();
    }

    private void initOnClickListener() {
        btnAddDirectory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!etName.getText().toString().equals("")) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            String dirName = etName.getText().toString();
                            Directory directory = new Directory();
                            directory.setName(dirName);
                            database.directoryDAO().insert(directory);
                        }
                    }).start();
                    etName.setText("");String dirName = etName.getText().toString();
                    Directory directory = new Directory();
                    directory.setName(dirName);
                    mActivity.directories.add(directory);
                }
            }
        });
    }

    private void setInitialData() {
        etName = getActivity().findViewById(R.id.et_directory_name);
        btnAddDirectory = getActivity().findViewById(R.id.btn_add_directory);
    }
}
