package com.example.noteapp.view.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.noteapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends BaseFragment {

    private Button btnNewDocument;
    private Button btnNewDirectory;
    private Button btnListDirectories;

    private static final String DATABASE_NAME = "office_db";

    public MainFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setInitialData();
        setOnClickListener();
    }

    private void setOnClickListener() {

        btnNewDocument.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeFragment(new NewNoteFragment());
            }
        });

        btnNewDirectory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeFragment(new NewDirectoryFragment());
            }
        });

        btnListDirectories.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeFragment(new ListDirectoriesFragment());
            }
        });
    }

    private void setInitialData() {
        btnNewDocument = getActivity().findViewById(R.id.btn_new_document);
        btnNewDirectory = getActivity().findViewById(R.id.btn_new_directory);
        btnListDirectories = getActivity().findViewById(R.id.btn_list_directories);
    }

}
