package com.example.noteapp.view.fragments;

import android.arch.persistence.room.Room;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import com.example.noteapp.MainActivity;
import com.example.noteapp.R;
import com.example.noteapp.model.db.AppDatabase;

public class BaseFragment extends Fragment {

    private FragmentTransaction transaction;
    public AppDatabase database;
    public MainActivity mActivity;
    private Thread loadData;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = new MainActivity();
        database = Room.databaseBuilder(getContext(),
                AppDatabase.class, "DATABASE")
                .fallbackToDestructiveMigration()
                .build();
        loadData = new Thread(new Runnable() {
            @Override
            public void run() {
                mActivity.directories.addAll(database.directoryDAO().getAllDirectories());
                mActivity.notes.addAll(database.noteDAO().getAllNotes());
            }
        });
        updateDatabase();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public void updateDatabase() {
        loadData.start();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        database.close();
    }

    public void changeFragment(Fragment newFragment) {
        transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.main_container, newFragment);
        transaction.addToBackStack("Back");
        transaction.commit();
    }
}
