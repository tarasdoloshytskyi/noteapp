package com.example.noteapp.view.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.noteapp.R;
import com.example.noteapp.model.entities.Directory;
import com.example.noteapp.model.entities.Note;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewNoteFragment extends BaseFragment {

    private Spinner spinnerSelectDirectory;
    private EditText etNoteName;
    private EditText etNoteBody;
    private Button btnAddNote;
    private int dirId = -1;
    String[] data;


    public NewNoteFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_new_document, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setInitialData();
        initSpinner();
        initOnClickListener();
    }

    private void initSpinner() {
        data = new String[mActivity.directories.size()];
        for (int i = 0; i < mActivity.directories.size(); i++) {
            data[i] = mActivity.directories.get(i).getName();
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, data);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerSelectDirectory.setAdapter(adapter);
        spinnerSelectDirectory.setPrompt("Select directory!");
        spinnerSelectDirectory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                Toast.makeText(getActivity(), "Position = " + position, Toast.LENGTH_SHORT).show();
                dirId = mActivity.directories.get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
    }

    private void initOnClickListener() {
        btnAddNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!etNoteName.getText().toString().equals("")
                        && !etNoteBody.getText().toString().equals("")
                        && dirId != -1) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            String noteName = etNoteName.getText().toString();
                            String noteBody = etNoteBody.getText().toString();
                            Note note = new Note();
                            note.setTitle(noteName);
                            note.setBody(noteBody);
                            note.setFolderId(dirId);
                            database.noteDAO().insertNote(note);
                        }
                    }).start();
                    String noteName = etNoteName.getText().toString();
                    String noteBody = etNoteBody.getText().toString();
                    Note note = new Note();
                    note.setTitle(noteName);
                    note.setBody(noteBody);
                    note.setFolderId(dirId);
                    mActivity.notes.add(note);
                    etNoteName.setText("");
                    etNoteBody.setText("");
                }
            }
        });
    }

    private void setInitialData() {
        spinnerSelectDirectory = getActivity().findViewById(R.id.spinner_select_directory);
        etNoteName = getActivity().findViewById(R.id.et_doc_name);
        etNoteBody = getActivity().findViewById(R.id.et_doc_body);
        btnAddNote = getActivity().findViewById(R.id.btn_add_document);
    }
}
