package com.example.noteapp.view.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.noteapp.R;
import com.example.noteapp.model.db.AppDatabase;
import com.example.noteapp.model.entities.Directory;
import com.example.noteapp.view.adapter.DirectoryAdapter;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListDirectoriesFragment extends BaseFragment {

    private RecyclerView rvListDirectories;
    private DirectoryAdapter adapter;

    public ListDirectoriesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list_directories, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rvListDirectories = getActivity().findViewById(R.id.rv_list_directories);
        rvListDirectories.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        rvListDirectories.setLayoutManager(layoutManager);
        adapter = new DirectoryAdapter(mActivity.directories);
        rvListDirectories.setAdapter(adapter);
        adapterClickListener();
    }


    private void adapterClickListener() {
        adapter.setOnItemClickListener(new DirectoryAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                changeFragment(new ListNotesFragment());
            }

            @Override
            public void onDeleteClick(int position) {
                Directory directory = mActivity.directories.get(position);
                deleteItem(position, directory);
            }
        });
    }

    private void deleteItem(final int position, final Directory directory) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                database.directoryDAO().delete(directory);
            }
        }).start();
        mActivity.directories.remove(position);
        adapter.notifyItemRemoved(position);
    }
}
