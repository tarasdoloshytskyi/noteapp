package com.example.noteapp.view.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.noteapp.R;
import com.example.noteapp.model.entities.Directory;
import com.example.noteapp.model.entities.Note;

import java.util.ArrayList;

public class NoteAdapter extends RecyclerView.Adapter<NoteAdapter.NoteViewHolder> {

    private ArrayList<Note> notes;
    private DirectoryAdapter.OnItemClickListener clickListener;

    public interface OnItemClickListener {
        void onItemClick(int position);

        void onDeleteClick(int position);
    }

    public class NoteViewHolder extends RecyclerView.ViewHolder{

        public ImageButton ibDelete;
        public TextView tvTitle;
        public TextView tvBody;

        public NoteViewHolder(@NonNull View itemView, final DirectoryAdapter.OnItemClickListener listener) {
            super(itemView);

            ibDelete = itemView.findViewById(R.id.ib_note_delete);
            tvTitle = itemView.findViewById(R.id.tv_note_title);
            tvBody = itemView.findViewById(R.id.tv_note_body);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }
                }
            });

            ibDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onDeleteClick(position);
                        }
                    }
                }
            });
        }
    }

    public void setOnItemClickListener(DirectoryAdapter.OnItemClickListener listener){
        clickListener = listener;
    }

    public NoteAdapter(ArrayList<Note> notes) {
        this.notes = notes;
    }

    @NonNull
    @Override
    public NoteViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.note_item, viewGroup, false);
        NoteAdapter.NoteViewHolder noteViewHolder = new NoteAdapter.NoteViewHolder(view, clickListener);
        return noteViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull NoteViewHolder noteViewHolder, int i) {
        Note note = notes.get(i);
        noteViewHolder.tvTitle.setText(note.getTitle());
        noteViewHolder.tvBody.setText(note.getBody());
    }

    @Override
    public int getItemCount() {
        return notes.size();
    }
}
