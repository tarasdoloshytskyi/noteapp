package com.example.noteapp;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;

import com.example.noteapp.model.db.AppDatabase;
import com.example.noteapp.model.entities.Directory;
import com.example.noteapp.model.entities.Note;
import com.example.noteapp.view.fragments.MainFragment;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    FrameLayout mainContainer;
    FragmentTransaction transaction;
    Fragment newFragment;
    public ArrayList<Directory> directories= new ArrayList<>();
    public ArrayList<Note> notes= new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setInitialData();
        setStartFragment();
    }

    @Override
    public View onCreateView(String name, Context context, AttributeSet attrs) {
        return super.onCreateView(name, context, attrs);
    }

    private void setStartFragment() {
        newFragment = new MainFragment();
        transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.main_container, newFragment);
        transaction.commit();
    }

    private void setInitialData() {
        mainContainer = findViewById(R.id.main_container);
    }
}
